﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

using UnityEngine;

public class DataBaseManager : MonoBehaviour
{

    public static DataBaseManager m_DataBaseManagerInstance;

    private string connectionString ;

    private StatisticsManager statisticsManager;
    public class Highscore
    {
        [BsonId]
        public int id { get; set; }
        public string Username { get; set; }
        public float Accuracy { get; set; }
        public int TotalRoundsWon { get; set; }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (m_DataBaseManagerInstance == null)
        {
            m_DataBaseManagerInstance = this;
        }
        else
        {
            Destroy(this);
        }

        connectionString = "URI=file:" + Application.dataPath + "/Resources/Database/MyGame.db";

        statisticsManager = GameObject.Find("StatisticsManager").GetComponent<StatisticsManager>();

    }

    public void Greeting()
    {
        Debug.Log("Hello from the Database Manager!");
    }

    public void CreateRoundData(int _roundNumber)
    {
        try
        {
            using (IDbConnection dbConnection = new SqliteConnection(connectionString))
            {
                dbConnection.Open();

                IDbCommand command = dbConnection.CreateCommand();
                command.CommandText = "INSERT INTO ROUND" + _roundNumber + " (PlayerFired, AIFired, TimeUsed, DistancePlayerMoved, DistanceAIMoved) VALUES (@PlayerFired, @AIFired, @TimeUsed, @DistancePlayerMoved, @DistanceAIMoved)";
                
                var parameter = command.CreateParameter();
                parameter.ParameterName = "@PlayerFired";
                parameter.Value = statisticsManager.m_PlayerFireCountByRound;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@AIFired";
                parameter.Value = statisticsManager.m_AIFireCountByRound;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@TimeUsed";
                parameter.Value = statisticsManager.m_TimeUsedByRound;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@DistancePlayerMoved";
                parameter.Value = statisticsManager.m_DistancePlayerMovedByRound;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@DistanceAIMoved";
                parameter.Value = statisticsManager.m_DistanceAIMovedByRound;
                command.Parameters.Add(parameter);

                command.ExecuteNonQuery();

                command.Dispose();
                dbConnection.Close();

                statisticsManager.ResetRoundData();
            }
        }
        catch (Exception ex)
        {

            Debug.LogError("Error updating round data: " + ex.Message);
            throw;
        }

        //Debug.Log("Round data created.");
    }

    public void UpdateGameData()
    {
        try
        {
            using (IDbConnection dbConnection = new SqliteConnection(connectionString))
            {
                dbConnection.Open();

                // Death Postion update
                foreach ( Vector2 location in statisticsManager.m_DeathLocation)
                {
                    IDbCommand command_deathpostion = dbConnection.CreateCommand();
                    command_deathpostion.CommandText = "INSERT INTO DEATHPOSITION" + " (Position_X, Position_Y) VALUES (@Position_X, @Position_Y)";

                    var parameter_deathpostion = command_deathpostion.CreateParameter();
                    parameter_deathpostion.ParameterName = "@Position_X";
                    parameter_deathpostion.Value = location.x;
                    command_deathpostion.Parameters.Add(parameter_deathpostion);

                    parameter_deathpostion = command_deathpostion.CreateParameter();
                    parameter_deathpostion.ParameterName = "@Position_Y";
                    parameter_deathpostion.Value = location.y;
                    command_deathpostion.Parameters.Add(parameter_deathpostion);

                    command_deathpostion.ExecuteNonQuery();

                    command_deathpostion.Dispose();
                }

                // Total Fire Update

                IDbCommand command = dbConnection.CreateCommand();
                command.CommandText = "INSERT INTO TOTALFIRE" + " (Player, AI) VALUES (@Player, @AI)";

                var parameter = command.CreateParameter();
                parameter.ParameterName = "@Player";
                parameter.Value = statisticsManager.m_PlayerFireCountTotal;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@AI";
                parameter.Value = statisticsManager.m_AIFireCountTotal;
                command.Parameters.Add(parameter);

                command.ExecuteNonQuery();

                command.Dispose();



                // All Time Data Update
                IDbCommand command_AllTime = dbConnection.CreateCommand();
                command_AllTime.CommandText = "UPDATE ALLTIMEDATA set PlayerFired = @PlayerFired, PlayerHit = @PlayerHit where id = 1";

                var parameter_AllTime = command_AllTime.CreateParameter();
                parameter_AllTime.ParameterName = "@PlayerFired";
                parameter_AllTime.Value = statisticsManager.m_AllTimePlayerFire + statisticsManager.m_PlayerFireCountTotal;
                command_AllTime.Parameters.Add(parameter_AllTime);

                parameter_AllTime = command_AllTime.CreateParameter();
                parameter_AllTime.ParameterName = "@PlayerHit";
                parameter_AllTime.Value = statisticsManager.m_AllTimePlayerHit + statisticsManager.m_PlayerHitTotal;
                command_AllTime.Parameters.Add(parameter_AllTime);

                command_AllTime.ExecuteNonQuery();

                command_AllTime.Dispose();


                dbConnection.Close();

            }
        }
        catch (Exception ex)
        {

            Debug.LogError("Error updating game data: " + ex.Message);
            throw ex;
        }

        //Debug.Log("Game data created.");
    }
    

    public static void ReadAllTimeData(StatisticsManager _instance)
    {
        using (IDbConnection dbConnection = new SqliteConnection("URI=file:" + Application.dataPath + "/Resources/Database/MyGame.db"))
        {
            dbConnection.Open();
            IDbCommand command = dbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM ALLTIMEDATA";

            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                _instance.m_AllTimePlayerFire = reader.GetInt32(1);
                _instance.m_AllTimePlayerHit = reader.GetInt32(2);

            }

            reader.Close();
            command.Dispose();

            dbConnection.Close();

        }

        
    }

    public void UpdateMongoDBRecord()
    {
        MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/<dbname>?retryWrites=true&w=majority");
        var db = client.GetDatabase("GDAP_Exercise");

        var myRecord = db.GetCollection<Highscore>("Highscores").Find(u => u.id == 991561534);

        int m_AllTimePlayerFire = 0;
        int m_AllTimePlayerHit = 0;

        // reading All time data from SQLite
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();
            IDbCommand command = dbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM ALLTIMEDATA";

            IDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                m_AllTimePlayerFire = reader.GetInt32(1);
                m_AllTimePlayerHit = reader.GetInt32(2);

            }

            reader.Close();
            command.Dispose();

            dbConnection.Close();

        }


        if (myRecord.CountDocuments() > 0)
        {
            Highscore highscore = myRecord.Single();

            highscore.id = 991561534;
            highscore.TotalRoundsWon += statisticsManager.m_TotalRoundsWon;

            highscore.Accuracy = (float)m_AllTimePlayerHit / m_AllTimePlayerFire;

            var coll = db.GetCollection<Highscore>("Highscores");
            
            coll.ReplaceOne(i => i.id == highscore.id, highscore);

        }

        //Debug.Log("MongoDB updated");
        //ReadMongoDatabase();
    }
   
}
