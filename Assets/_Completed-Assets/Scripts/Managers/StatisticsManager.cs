﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatisticsManager : MonoBehaviour
{
    public static StatisticsManager m_StatisticsManagerInstance;

    // All time Data
    public int m_AllTimePlayerFire;
    public int m_AllTimePlayerHit;

    // Game Data
    public List<Vector2> m_DeathLocation = new List<Vector2>(); 
    public int m_PlayerHitTotal = 0;    
    public int m_PlayerFireCountTotal = 0;   
    public int m_AIFireCountTotal = 0;  
    public int m_TotalRoundsWon = 0;    


    // Round Data
    public float m_DistancePlayerMovedByRound = 0.0f;   
    public float m_DistanceAIMovedByRound = 0.0f;   
    public int m_PlayerFireCountByRound = 0;    
    public int m_AIFireCountByRound = 0;    
    public float m_TimeUsedByRound = 0.0f;  

    // Start is called before the first frame update
    void Start()
    {
        if (m_StatisticsManagerInstance == null)
        {
            m_StatisticsManagerInstance = this;
        }
        else
        {
            Destroy(this);
        }

        DataBaseManager.ReadAllTimeData(m_StatisticsManagerInstance);
    }

    public void ResetRoundData()
    {
        m_DistancePlayerMovedByRound = 0.0f;
        m_DistanceAIMovedByRound = 0.0f;
        m_PlayerFireCountByRound = 0;
        m_AIFireCountByRound = 0;
        m_TimeUsedByRound = 0.0f;
}

    public void AddDistance(float _movement, bool _isPlayer)
    {
        if (_isPlayer)
        {
            m_DistancePlayerMovedByRound += _movement;
        }
        else
        {
            m_DistanceAIMovedByRound += _movement;
        }

        //Debug.Log(m_DistanceAIMovedByRound);
    }

    public void AddDeathLocation(Vector2 _position)
    {
        m_DeathLocation.Add(_position);

    }

    public void AddFireCount(bool _isPlayer)
    {
        if (_isPlayer)
        {
            m_PlayerFireCountByRound++;
            m_PlayerFireCountTotal++;
        }
        else
        {
            m_AIFireCountByRound++;
            m_AIFireCountTotal++;
        }

        
    }

    public void AddPlayerHit()
    {
        m_PlayerHitTotal++;
        Debug.Log(m_PlayerHitTotal);
    }
    

    public void RecordTimeUsed(float _time)
    {
        m_TimeUsedByRound = _time;

    }

    public void AddRoundWins(int _amount)
    {
        m_TotalRoundsWon += _amount;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
