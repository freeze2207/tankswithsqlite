﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualizeData_Winform
{
    public partial class Form1 : Form
    {
        
        private static string DatabasePath = "./../../../Assets/Resources/Database/MyGame.db";

        private static string MongoDBConnectionString = "mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/<dbname>?retryWrites=true&w=majority";

        private List<Vector2> m_deathPoints = new List<Vector2>();
        private int m_playerTotalShoots = 0;
        private int m_AITotalShoots = 0;

        private float m_round1AvePlayerShoots = 0.0f;
        private float m_round1AveAIShoots = 0.0f;
        private float m_round1AveTime = 0.0f;
        private float m_round1AvePlayerDistance = 0.0f;
        private float m_round1AveAIDistance = 0.0f;

        private float m_round2AvePlayerShoots = 0.0f;
        private float m_round2AveAIShoots = 0.0f;
        private float m_round2AveTime = 0.0f;
        private float m_round2AvePlayerDistance = 0.0f;
        private float m_round2AveAIDistance = 0.0f;

        private float m_round3AvePlayerShoots = 0.0f;
        private float m_round3AveAIShoots = 0.0f;
        private float m_round3AveTime = 0.0f;
        private float m_round3AvePlayerDistance = 0.0f;
        private float m_round3AveAIDistance = 0.0f;


        public class Highscore
        {
            [BsonId]
            public int id { get; set; }
            public string Username { get; set; }
            public float Accuracy { get; set; }
            public int TotalRoundsWon { get; set; }
        }     

        public static int CompareByRoundsWon(Highscore x, Highscore y)
        {
            if (x.TotalRoundsWon < y.TotalRoundsWon)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public Form1()
        {
            InitializeComponent();

            SQLite_ReadData();

            ReadMongoDatabase();

            GenerateHeatmap(m_deathPoints);

            PlayShootsValue.Text = m_playerTotalShoots.ToString();
            AIShootsValue.Text = m_AITotalShoots.ToString();

            groupBox_radio.Controls.Add(radioButton_round1);
            groupBox_radio.Controls.Add(radioButton_round2);
            groupBox_radio.Controls.Add(radioButton_round3);

            radioButton_round1.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            radioButton_round2.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            radioButton_round3.CheckedChanged += new EventHandler(radioButton_CheckedChanged);

            

        }

        private void GenerateHeatmap(List<Vector2> _deathPoints)
        {

            Bitmap pic = new Bitmap(heatMapPicBox.Width, heatMapPicBox.Height);

            foreach (Vector2 point in _deathPoints)
            {
                // Based on death coordinates tansform to 512x512 bit map scale

                DrawDeathPoint(pic, (int)Math.Round(point.X * 5.12 + 256), (int)Math.Round(point.Y * 5.12 + 256));
            }

            
            heatMapPicBox.Image = pic;
        }

        private void DrawDeathPoint(Bitmap _bitmap, int x, int y)
        {
            if ( x + 2 > 511 || y + 2 > 511)
            {
                return;
            }
            else
            {
                _bitmap.SetPixel(x, y, Color.FromArgb(255, Color.Red));
                _bitmap.SetPixel(x + 1, y + 1, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 1, y - 1, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x, y + 1, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x, y - 1, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x + 1, y, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 1, y, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x + 1, y - 1, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 1, y + 1, Color.FromArgb(100, Color.Red));

                _bitmap.SetPixel(x + 2, y + 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 2, y - 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 1, y - 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x, y - 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x + 1, y - 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x + 2, y - 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x + 2, y - 1, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x + 2, y, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x + 2, y + 1, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x + 1, y + 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x, y + 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 1, y + 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 2, y + 2, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 2, y + 1, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 2, y, Color.FromArgb(100, Color.Red));
                _bitmap.SetPixel(x - 2, y + 1, Color.FromArgb(100, Color.Red));
            }
            

        }

        private void SQLite_ReadData()
        {
            DataTable deathPointsTable = new DataTable();
            DataTable totalFireTable = new DataTable();

            DataTable round1Table = new DataTable();
            DataTable round2Table = new DataTable();
            DataTable round3Table = new DataTable();

            List<Vector2> list = new List<Vector2>();

            try
            {
                using (SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + DatabasePath))
                {
                    dbConnection.Open();

                    SQLiteDataAdapter adapter = new SQLiteDataAdapter("SELECT * from DeathPosition", dbConnection);
                    adapter.Fill(deathPointsTable);

                    adapter = new SQLiteDataAdapter("SELECT * from TotalFire", dbConnection);
                    adapter.Fill(totalFireTable);

                    adapter = new SQLiteDataAdapter("SELECT * from Round1", dbConnection);
                    adapter.Fill(round1Table);
                    adapter = new SQLiteDataAdapter("SELECT * from Round2", dbConnection);
                    adapter.Fill(round2Table);
                    adapter = new SQLiteDataAdapter("SELECT * from Round3", dbConnection);
                    adapter.Fill(round3Table);

                    dbConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Reading data: " + ex.Message);
            }


            foreach (DataRow row in deathPointsTable.Rows)
            {
                m_deathPoints.Add(new Vector2(System.Convert.ToSingle( row["Position_X"]), System.Convert.ToSingle(row["Position_Y"])));
            }

            foreach (DataRow row in totalFireTable.Rows)
            {
                m_playerTotalShoots += System.Convert.ToInt32(row["Player"]);
                m_AITotalShoots += System.Convert.ToInt32(row["AI"]);
            }


            for (int i = 0; i < round1Table.Rows.Count; i ++)
            {
                m_round1AvePlayerShoots += System.Convert.ToSingle(round1Table.Rows[i]["PlayerFired"]);
                m_round1AveAIShoots += System.Convert.ToSingle(round1Table.Rows[i]["AIFired"]);
                m_round1AveTime += System.Convert.ToSingle(round1Table.Rows[i]["TimeUsed"]);
                m_round1AvePlayerDistance += System.Convert.ToSingle(round1Table.Rows[i]["DistancePlayerMoved"]);
                m_round1AveAIDistance += System.Convert.ToSingle(round1Table.Rows[i]["DistanceAIMoved"]);
            }

            m_round1AvePlayerShoots /= round1Table.Rows.Count;
            m_round1AveAIShoots /= round1Table.Rows.Count;
            m_round1AveTime /= round1Table.Rows.Count;
            m_round1AvePlayerDistance /= round1Table.Rows.Count;
            m_round1AveAIDistance /= round1Table.Rows.Count;


            for (int i = 0; i < round2Table.Rows.Count; i++)
            {
                m_round2AvePlayerShoots += System.Convert.ToSingle(round2Table.Rows[i]["PlayerFired"]);
                m_round2AveAIShoots += System.Convert.ToSingle(round2Table.Rows[i]["AIFired"]);
                m_round2AveTime += System.Convert.ToSingle(round2Table.Rows[i]["TimeUsed"]);
                m_round2AvePlayerDistance += System.Convert.ToSingle(round2Table.Rows[i]["DistancePlayerMoved"]);
                m_round2AveAIDistance += System.Convert.ToSingle(round2Table.Rows[i]["DistanceAIMoved"]);
            }

            m_round2AvePlayerShoots /= round2Table.Rows.Count;
            m_round2AveAIShoots /= round2Table.Rows.Count;
            m_round2AveTime /= round2Table.Rows.Count;
            m_round2AvePlayerDistance /= round2Table.Rows.Count;
            m_round2AveAIDistance /= round2Table.Rows.Count;


            for (int i = 0; i < round3Table.Rows.Count; i++)
            {
                m_round3AvePlayerShoots += System.Convert.ToSingle(round3Table.Rows[i]["PlayerFired"]);
                m_round3AveAIShoots += System.Convert.ToSingle(round3Table.Rows[i]["AIFired"]);
                m_round3AveTime += System.Convert.ToSingle(round3Table.Rows[i]["TimeUsed"]);
                m_round3AvePlayerDistance += System.Convert.ToSingle(round3Table.Rows[i]["DistancePlayerMoved"]);
                m_round3AveAIDistance += System.Convert.ToSingle(round3Table.Rows[i]["DistanceAIMoved"]);
            }
            Console.WriteLine(m_round3AvePlayerShoots);

            m_round3AvePlayerShoots /= round3Table.Rows.Count;
            m_round3AveAIShoots /= round3Table.Rows.Count;
            m_round3AveTime /= round3Table.Rows.Count;
            m_round3AvePlayerDistance /= round3Table.Rows.Count;
            m_round3AveAIDistance /= round3Table.Rows.Count;
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;

            if (rb == null)
            {
                MessageBox.Show("Sender is not a RadioButton");
                return;
            }

            if (rb.Checked)
            {
                
                switch (rb.Name)
                {
                    case "radioButton_round1":
                        PlayerShootsAveValue.Text = m_round1AvePlayerShoots.ToString();
                        AIShootsAveValue.Text = m_round1AveAIShoots.ToString();
                        AveRoundTimeValue.Text = m_round1AveTime.ToString();
                        PlayerDistanceValue.Text = m_round1AvePlayerDistance.ToString();
                        AIDistanceValue.Text = m_round1AveAIDistance.ToString();
                        break;
                    case "radioButton_round2":
                        PlayerShootsAveValue.Text = m_round2AvePlayerShoots.ToString();
                        AIShootsAveValue.Text = m_round2AveAIShoots.ToString();
                        AveRoundTimeValue.Text = m_round2AveTime.ToString();
                        PlayerDistanceValue.Text = m_round2AvePlayerDistance.ToString();
                        AIDistanceValue.Text = m_round2AveAIDistance.ToString();
                        break;
                    case "radioButton_round3":
                        PlayerShootsAveValue.Text = m_round3AvePlayerShoots.ToString();
                        AIShootsAveValue.Text = m_round3AveAIShoots.ToString();
                        AveRoundTimeValue.Text = m_round3AveTime.ToString();
                        PlayerDistanceValue.Text = m_round3AvePlayerDistance.ToString();
                        AIDistanceValue.Text = m_round3AveAIDistance.ToString();
                        break;
                    default:
                        break;
                }

            }
        }



        void ReadMongoDatabase()
        {
            MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("GDAP_Exercise");

            var collection = db.GetCollection<Highscore>("Highscores").Find(_ => true).ToList();


            collection.Sort(CompareByRoundsWon);

            foreach (var highScore in collection)
            {
                
                ListViewItem item = new ListViewItem(highScore.Username, 0);
                item.SubItems.Add(highScore.TotalRoundsWon.ToString());

                leaderBoard.Items.Add(item);
            }

        }


    }
}
