﻿namespace VisualizeData_Winform
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.heatMapPicBox = new System.Windows.Forms.PictureBox();
            this.PlayShootsName = new System.Windows.Forms.Label();
            this.PlayShootsValue = new System.Windows.Forms.Label();
            this.AIShootsValue = new System.Windows.Forms.Label();
            this.AIShootsLabel = new System.Windows.Forms.Label();
            this.radioButton_round1 = new System.Windows.Forms.RadioButton();
            this.radioButton_round2 = new System.Windows.Forms.RadioButton();
            this.radioButton_round3 = new System.Windows.Forms.RadioButton();
            this.AIShootsAveValue = new System.Windows.Forms.Label();
            this.AIAveShootsLabel = new System.Windows.Forms.Label();
            this.PlayerShootsAveValue = new System.Windows.Forms.Label();
            this.PlayerAveShootsLabel = new System.Windows.Forms.Label();
            this.AIDistanceValue = new System.Windows.Forms.Label();
            this.AIDistanceLabel = new System.Windows.Forms.Label();
            this.AveRoundTimeValue = new System.Windows.Forms.Label();
            this.AveRoundTimeLabel = new System.Windows.Forms.Label();
            this.PlayerDistanceValue = new System.Windows.Forms.Label();
            this.PlayerDistanceLabel = new System.Windows.Forms.Label();
            this.groupBox_radio = new System.Windows.Forms.GroupBox();
            this.leaderBoard = new System.Windows.Forms.ListView();
            this.Username = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TotalRoundsWon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.heatMapPicBox)).BeginInit();
            this.groupBox_radio.SuspendLayout();
            this.SuspendLayout();
            // 
            // heatMapPicBox
            // 
            this.heatMapPicBox.BackgroundImage = global::VisualizeData_Winform.Properties.Resources.tanks_map;
            this.heatMapPicBox.Location = new System.Drawing.Point(12, 12);
            this.heatMapPicBox.Name = "heatMapPicBox";
            this.heatMapPicBox.Size = new System.Drawing.Size(512, 512);
            this.heatMapPicBox.TabIndex = 0;
            this.heatMapPicBox.TabStop = false;
            // 
            // PlayShootsName
            // 
            this.PlayShootsName.AutoSize = true;
            this.PlayShootsName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayShootsName.Location = new System.Drawing.Point(665, 22);
            this.PlayShootsName.Name = "PlayShootsName";
            this.PlayShootsName.Size = new System.Drawing.Size(176, 20);
            this.PlayShootsName.TabIndex = 1;
            this.PlayShootsName.Text = "Total Shoots By Player: ";
            // 
            // PlayShootsValue
            // 
            this.PlayShootsValue.AutoSize = true;
            this.PlayShootsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayShootsValue.Location = new System.Drawing.Point(665, 51);
            this.PlayShootsValue.Name = "PlayShootsValue";
            this.PlayShootsValue.Size = new System.Drawing.Size(113, 20);
            this.PlayShootsValue.TabIndex = 2;
            this.PlayShootsValue.Text = "Shoots value";
            // 
            // AIShootsValue
            // 
            this.AIShootsValue.AutoSize = true;
            this.AIShootsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AIShootsValue.Location = new System.Drawing.Point(665, 131);
            this.AIShootsValue.Name = "AIShootsValue";
            this.AIShootsValue.Size = new System.Drawing.Size(113, 20);
            this.AIShootsValue.TabIndex = 4;
            this.AIShootsValue.Text = "Shoots value";
            // 
            // AIShootsLabel
            // 
            this.AIShootsLabel.AutoSize = true;
            this.AIShootsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AIShootsLabel.Location = new System.Drawing.Point(665, 102);
            this.AIShootsLabel.Name = "AIShootsLabel";
            this.AIShootsLabel.Size = new System.Drawing.Size(149, 20);
            this.AIShootsLabel.TabIndex = 3;
            this.AIShootsLabel.Text = "Total Shoots By AI: ";
            // 
            // radioButton_round1
            // 
            this.radioButton_round1.AutoSize = true;
            this.radioButton_round1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_round1.Location = new System.Drawing.Point(6, 17);
            this.radioButton_round1.Name = "radioButton_round1";
            this.radioButton_round1.Size = new System.Drawing.Size(88, 24);
            this.radioButton_round1.TabIndex = 5;
            this.radioButton_round1.TabStop = true;
            this.radioButton_round1.Text = "Round 1";
            this.radioButton_round1.UseVisualStyleBackColor = true;
            // 
            // radioButton_round2
            // 
            this.radioButton_round2.AutoSize = true;
            this.radioButton_round2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_round2.Location = new System.Drawing.Point(100, 17);
            this.radioButton_round2.Name = "radioButton_round2";
            this.radioButton_round2.Size = new System.Drawing.Size(88, 24);
            this.radioButton_round2.TabIndex = 6;
            this.radioButton_round2.TabStop = true;
            this.radioButton_round2.Text = "Round 2";
            this.radioButton_round2.UseVisualStyleBackColor = true;
            // 
            // radioButton_round3
            // 
            this.radioButton_round3.AutoSize = true;
            this.radioButton_round3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_round3.Location = new System.Drawing.Point(194, 17);
            this.radioButton_round3.Name = "radioButton_round3";
            this.radioButton_round3.Size = new System.Drawing.Size(88, 24);
            this.radioButton_round3.TabIndex = 7;
            this.radioButton_round3.TabStop = true;
            this.radioButton_round3.Text = "Round 3";
            this.radioButton_round3.UseVisualStyleBackColor = true;
            // 
            // AIShootsAveValue
            // 
            this.AIShootsAveValue.AutoSize = true;
            this.AIShootsAveValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AIShootsAveValue.Location = new System.Drawing.Point(777, 268);
            this.AIShootsAveValue.Name = "AIShootsAveValue";
            this.AIShootsAveValue.Size = new System.Drawing.Size(113, 20);
            this.AIShootsAveValue.TabIndex = 11;
            this.AIShootsAveValue.Text = "Shoots value";
            // 
            // AIAveShootsLabel
            // 
            this.AIAveShootsLabel.AutoSize = true;
            this.AIAveShootsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AIAveShootsLabel.Location = new System.Drawing.Point(777, 239);
            this.AIAveShootsLabel.Name = "AIAveShootsLabel";
            this.AIAveShootsLabel.Size = new System.Drawing.Size(173, 20);
            this.AIAveShootsLabel.TabIndex = 10;
            this.AIAveShootsLabel.Text = "Average Shoots By AI: ";
            // 
            // PlayerShootsAveValue
            // 
            this.PlayerShootsAveValue.AutoSize = true;
            this.PlayerShootsAveValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerShootsAveValue.Location = new System.Drawing.Point(571, 268);
            this.PlayerShootsAveValue.Name = "PlayerShootsAveValue";
            this.PlayerShootsAveValue.Size = new System.Drawing.Size(113, 20);
            this.PlayerShootsAveValue.TabIndex = 9;
            this.PlayerShootsAveValue.Text = "Shoots value";
            // 
            // PlayerAveShootsLabel
            // 
            this.PlayerAveShootsLabel.AutoSize = true;
            this.PlayerAveShootsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerAveShootsLabel.Location = new System.Drawing.Point(571, 239);
            this.PlayerAveShootsLabel.Name = "PlayerAveShootsLabel";
            this.PlayerAveShootsLabel.Size = new System.Drawing.Size(200, 20);
            this.PlayerAveShootsLabel.TabIndex = 8;
            this.PlayerAveShootsLabel.Text = "Average Shoots By Player: ";
            // 
            // AIDistanceValue
            // 
            this.AIDistanceValue.AutoSize = true;
            this.AIDistanceValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AIDistanceValue.Location = new System.Drawing.Point(571, 512);
            this.AIDistanceValue.Name = "AIDistanceValue";
            this.AIDistanceValue.Size = new System.Drawing.Size(51, 20);
            this.AIDistanceValue.TabIndex = 15;
            this.AIDistanceValue.Text = "value";
            // 
            // AIDistanceLabel
            // 
            this.AIDistanceLabel.AutoSize = true;
            this.AIDistanceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AIDistanceLabel.Location = new System.Drawing.Point(571, 483);
            this.AIDistanceLabel.Name = "AIDistanceLabel";
            this.AIDistanceLabel.Size = new System.Drawing.Size(236, 20);
            this.AIDistanceLabel.TabIndex = 14;
            this.AIDistanceLabel.Text = "Average Distance Moved By AI: ";
            // 
            // AveRoundTimeValue
            // 
            this.AveRoundTimeValue.AutoSize = true;
            this.AveRoundTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AveRoundTimeValue.Location = new System.Drawing.Point(571, 345);
            this.AveRoundTimeValue.Name = "AveRoundTimeValue";
            this.AveRoundTimeValue.Size = new System.Drawing.Size(113, 20);
            this.AveRoundTimeValue.TabIndex = 13;
            this.AveRoundTimeValue.Text = "Shoots value";
            // 
            // AveRoundTimeLabel
            // 
            this.AveRoundTimeLabel.AutoSize = true;
            this.AveRoundTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AveRoundTimeLabel.Location = new System.Drawing.Point(571, 316);
            this.AveRoundTimeLabel.Name = "AveRoundTimeLabel";
            this.AveRoundTimeLabel.Size = new System.Drawing.Size(166, 20);
            this.AveRoundTimeLabel.TabIndex = 12;
            this.AveRoundTimeLabel.Text = "Average Round Time: ";
            // 
            // PlayerDistanceValue
            // 
            this.PlayerDistanceValue.AutoSize = true;
            this.PlayerDistanceValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerDistanceValue.Location = new System.Drawing.Point(571, 431);
            this.PlayerDistanceValue.Name = "PlayerDistanceValue";
            this.PlayerDistanceValue.Size = new System.Drawing.Size(51, 20);
            this.PlayerDistanceValue.TabIndex = 17;
            this.PlayerDistanceValue.Text = "value";
            // 
            // PlayerDistanceLabel
            // 
            this.PlayerDistanceLabel.AutoSize = true;
            this.PlayerDistanceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerDistanceLabel.Location = new System.Drawing.Point(571, 402);
            this.PlayerDistanceLabel.Name = "PlayerDistanceLabel";
            this.PlayerDistanceLabel.Size = new System.Drawing.Size(263, 20);
            this.PlayerDistanceLabel.TabIndex = 16;
            this.PlayerDistanceLabel.Text = "Average Distance Moved By Player: ";
            // 
            // groupBox_radio
            // 
            this.groupBox_radio.Controls.Add(this.radioButton_round1);
            this.groupBox_radio.Controls.Add(this.radioButton_round2);
            this.groupBox_radio.Controls.Add(this.radioButton_round3);
            this.groupBox_radio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_radio.Location = new System.Drawing.Point(575, 173);
            this.groupBox_radio.Name = "groupBox_radio";
            this.groupBox_radio.Size = new System.Drawing.Size(375, 47);
            this.groupBox_radio.TabIndex = 18;
            this.groupBox_radio.TabStop = false;
            this.groupBox_radio.Text = "Select Round Spec Data";
            // 
            // leaderBoard
            // 
            this.leaderBoard.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Username,
            this.TotalRoundsWon});
            this.leaderBoard.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leaderBoard.HideSelection = false;
            this.leaderBoard.Location = new System.Drawing.Point(298, 595);
            this.leaderBoard.Name = "leaderBoard";
            this.leaderBoard.Size = new System.Drawing.Size(559, 322);
            this.leaderBoard.TabIndex = 19;
            this.leaderBoard.UseCompatibleStateImageBehavior = false;
            this.leaderBoard.View = System.Windows.Forms.View.Details;
            // 
            // Username
            // 
            this.Username.Text = "Username";
            this.Username.Width = 248;
            // 
            // TotalRoundsWon
            // 
            this.TotalRoundsWon.Text = "TotalRoundsWon";
            this.TotalRoundsWon.Width = 245;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1081, 1033);
            this.Controls.Add(this.leaderBoard);
            this.Controls.Add(this.groupBox_radio);
            this.Controls.Add(this.PlayerDistanceValue);
            this.Controls.Add(this.PlayerDistanceLabel);
            this.Controls.Add(this.AIDistanceValue);
            this.Controls.Add(this.AIDistanceLabel);
            this.Controls.Add(this.AveRoundTimeValue);
            this.Controls.Add(this.AveRoundTimeLabel);
            this.Controls.Add(this.AIShootsAveValue);
            this.Controls.Add(this.AIAveShootsLabel);
            this.Controls.Add(this.PlayerShootsAveValue);
            this.Controls.Add(this.PlayerAveShootsLabel);
            this.Controls.Add(this.AIShootsValue);
            this.Controls.Add(this.AIShootsLabel);
            this.Controls.Add(this.PlayShootsValue);
            this.Controls.Add(this.PlayShootsName);
            this.Controls.Add(this.heatMapPicBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.heatMapPicBox)).EndInit();
            this.groupBox_radio.ResumeLayout(false);
            this.groupBox_radio.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox heatMapPicBox;
        private System.Windows.Forms.Label PlayShootsName;
        private System.Windows.Forms.Label PlayShootsValue;
        private System.Windows.Forms.Label AIShootsValue;
        private System.Windows.Forms.Label AIShootsLabel;
        private System.Windows.Forms.RadioButton radioButton_round1;
        private System.Windows.Forms.RadioButton radioButton_round2;
        private System.Windows.Forms.RadioButton radioButton_round3;
        private System.Windows.Forms.Label AIShootsAveValue;
        private System.Windows.Forms.Label AIAveShootsLabel;
        private System.Windows.Forms.Label PlayerShootsAveValue;
        private System.Windows.Forms.Label PlayerAveShootsLabel;
        private System.Windows.Forms.Label AIDistanceValue;
        private System.Windows.Forms.Label AIDistanceLabel;
        private System.Windows.Forms.Label AveRoundTimeValue;
        private System.Windows.Forms.Label AveRoundTimeLabel;
        private System.Windows.Forms.Label PlayerDistanceValue;
        private System.Windows.Forms.Label PlayerDistanceLabel;
        private System.Windows.Forms.GroupBox groupBox_radio;
        private System.Windows.Forms.ListView leaderBoard;
        private System.Windows.Forms.ColumnHeader Username;
        private System.Windows.Forms.ColumnHeader TotalRoundsWon;
    }
}

